var Sistema = 
{
    initialize: function()
    {
        Sistema.initTable();
        Sistema.initForm();
        Sistema.initContainer();
        //Sistema.initMenu();
        
        $j('#exit').button({
            icons: {
                primary: "ui-icon ui-icon-closethick"
            },
            text: false
        });
        $j('.toggle').button({
            icons: {
                primary: "ui-icon ui-icon-circle-plus"
            },
            text: false
        }).click(function() 
        {
            var options;
            if ( $j(this).text() === "Ocultar" ) 
            {
                options = {
                    label: "Exibir",
                    icons: {
                        primary: "ui-icon ui-icon-circle-minus"
                    }
                };
            } 
            else 
            {
                options = {
                    label: "Ocultar",
                    icons: {
                        primary: "ui-icon ui-icon-circle-plus"
                    }
                };
            }
            $j( this ).button( "option", options );
        });
        $j(".toolbar").addClass("ui-state-default ui-corner-all");
        
    },
    initTable: function()
    {
        $j("table.data th").addClass("ui-state-default");
        $j("table.data td").addClass("ui-widget-content");
        
        $j('table.data td .actionList').each(function (i) {
            var icon = $j(this).attr('icon') || "";
            $j(this).button({
                icons: {
                    primary: icon
                },
                text: false
            });
        });
        
        $j("table.data tr").hover(
            function()
            {
                $j(this).children("td").addClass("ui-state-hover");
            },
            function()
            {
                $j(this).children("td").removeClass("ui-state-hover");
            }
            );

        $j("table.data tr").click(function(){
            $j(this).children("td").toggleClass("ui-state-highlight");
        });
        
        $j("#navgator" ).buttonset();
        $j(".noTextLeft").button({
            text: false,
            icons: {
                primary: "ui-icon-circle-triangle-w"
            }
        });
        $j(".noTextRight").button({
            text: false,
            icons: {
                primary: "ui-icon-circle-triangle-e"
            }
        });
        
        $j('.data tr').find('td.tdData:last').addClass('tdLastCol');
        $j('.data tr:last').find('td.tdData').addClass('tdLastRow');
        $j('.data tr:last').find('td.tdData:last').addClass('tdLast');
        $j('.data tr th').addClass('thFirst');
        $j('.data tr th:last').addClass('thLast');
        
    },
    initForm: function()
    {
        $j(".inputDefault").addClass("ui-widget ui-corner-all ui-state-default");
        $j(".inputDefault:disabled").addClass("ui-state-disabled");
        $j('.toolbar button').each(function (i) {
            var icon = $j(this).attr('icon') || "";
            $j(this).button({
                icons: {
                    primary: icon
                }
            });
        });
        $j('.checkDefault').button();
        $j('select').selectmenu();
        $j('.ui-selectmenu-menu-dropdown').height("auto");
    },
    initContainer: function()
    {
        $j(".container").addClass("ui-tabs ui-widget ui-widget-content ui-corner-all");
        Sistema.initContainerBody();
        Sistema.initContainerHead();
    },
    initContainerHead: function()
    {
        $j(".containerHead").addClass("ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all");
    },
    initContainerBody: function()
    {
        $j(".containerBody").addClass("ui-tabs-panel ui-widget-content ui-corner-bottom");
    }/*,
    initMenu: function()
    {
        $j("#menu").accordion({
            event       : "mouseover",
            autoHeight  : false
        });
        $j('#menu .contentLink a').button({
            icons: {
                primary : "ui-icon-play"
            }
        });
        $j("#menu .contentLink a").css({
            "width": "100%"
        });
        $j("#menu .contentLink a .ui-button-text").css({
            "font-size": "12px"
        });
    }*/
};