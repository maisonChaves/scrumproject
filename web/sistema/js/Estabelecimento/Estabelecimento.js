var Estabelecimento = {
    latitude: -12.043333,
    longitude: -77.028333,
    map: null,
    initialize: function()
    {
        Estabelecimento.map = new GMaps({
            div: '#mapContainer',
            lat: Estabelecimento.latitude,
            lng: Estabelecimento.longitude
        });
        
        Estabelecimento.map.addMarker({
            lat: -12.043333,
            lng: -77.028333,
            draggable: true,
            title: 'Lima',
            //click: function(e) {
                //alert('You clicked in this marker');
            //},
            dragend: function (e)
            {
                $j("#latitude").val(e.latLng.lat());
                $j("#longitude").val(e.latLng.lng());
            }
        });
    }
};