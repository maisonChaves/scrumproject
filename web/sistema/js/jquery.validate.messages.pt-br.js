/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: PT_BR
 */
jQuery.extend(jQuery.validator.messages, {
	required: "Este campo é obrigatorio.",
	remote: "Atenção, corrija este campo.",
	email: "Atenção, forneça um e-mail válido.",
	url: "Atenção, forneça uma URL válida.",
	date: "Atenção, forneça uma data válida.",
	dateISO: "Atenção, forneça uma data válida (ISO).",
	number: "Atenção, forneça um número válido.",
	digits: "Atenção, forneça somente dígitos.",
	creditcard: "Atenção, forneça um cartão de crédito válido.",
	equalTo: "Atenção, forneça o mesmo valor novamente.",
	accept: "Atenção, forneça um valor com uma extensão válida.",
	maxlength: jQuery.validator.format("Atenção, forneça não mais que {0} caracteres."),
	minlength: jQuery.validator.format("Atenção, forneça ao menos {0} caracteres."),
	rangelength: jQuery.validator.format("Atenção, forneça um valor entre {0} e {1} caracteres de comprimento."),
	range: jQuery.validator.format("Atenção, forneça um valor entre {0} e {1}."),
	max: jQuery.validator.format("Atenção, forneça um valor menor ou igual a {0}."),
	min: jQuery.validator.format("Atenção, forneça um valor maior ou igual a {0}.")
});
