<%-- 
    Document   : main
    Created on : 16/01/2012, 08:31:37
    Author     : Maisn Chaves
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%@attribute name="lista" type="java.util.List" required="true"%>
<%@attribute name="titulo" type="java.lang.String" required="true"%>
<%@attribute name="vazio" type="java.lang.String" required="true"%>
<%@attribute name="total" type="java.lang.Integer" %>
<%@attribute name="page" type="java.lang.Integer" %>
<%@attribute name="pageSize" type="java.lang.Integer" %>
<%@attribute name="totalPage" type="java.lang.Integer" %>
<jsp:doBody var="body" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ovc" tagdir="/WEB-INF/tags/" %>

<ovc:container nome="containerList" titulo="${titulo}" >
    <c:choose>
        <c:when test="${total == 0}">
            <div style="margin: 10px 0; padding: .7em .7em;" class="ui-state-highlight ui-corner-all"> 
                <p>
                    <span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-info"></span>
                    ${vazio}
                </p>
            </div>
        </c:when>
        <c:otherwise>
            <p>
                Pagina <b>${page}</b> de <b>${totalPage}</b>.
            </p>
            <p>
                <b>${total}</b> itens encontrados.
            </p>
            ${body}
            <c:if test="${total > pageSize}">
                <div class="toolbar" align="center">
                    <span id="navgator">
                        <button onclick="Navigator.prev();" class="noTextLeft">Voltar</button>
                        <c:forEach  var="i" begin="1" end="${totalPage}">  
                            <input onclick="Navigator.goTo(${i});" name="navBtn" type="radio" id="check${i}" <c:if test="${i == page}" >checked</c:if>/>
                            <label for="check${i}">${i}</label>
                        </c:forEach> 
                        <button onclick="Navigator.next();" class="noTextRight">Avançar</button>
                    </span>
                </div>
            </c:if>
        </c:otherwise>
    </c:choose>
</ovc:container>