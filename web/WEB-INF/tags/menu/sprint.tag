<%--
    Document   : sprint
    Created on : 16/01/2012, 08:31:37
    Author     : Maisn Chaves
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%@attribute name="projetoId" type="java.lang.Integer" required="true"%>
<%@attribute name="sprintId" type="java.lang.Integer" required="true"%>
<%@attribute name="numero" type="java.lang.String" required="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<li>
    <a href='<c:url value="/projeto/${projetoId}/sprint/${sprintId}/historia/lista"/>'>Sprint ${numero}</a>
    <ul>
        <li><a href='<c:url value="/projeto/${projetoId}/sprint/${sprintId}/historia/lista"/>'>Historia</a></li>
        <li><a href='<c:url value="/sprint/${sprintId}/controleAlteracao"/>'>Controle de Alteração</a></li>
        <li><a href="<c:url value="/sprint/${sprintId}/kanban"/>">KanBan</a></li>
    </ul>
</li>

