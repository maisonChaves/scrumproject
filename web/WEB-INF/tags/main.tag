<%-- 
    Document   : main
    Created on : 16/01/2012, 08:31:37
    Author     : Maisn Chaves
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%@attribute name="menu" type="java.lang.Boolean" required="true"%>
<%@attribute name="javaScript" type="java.lang.String"%>
<%@attribute name="initMethod" type="java.lang.String"%>
<%@attribute name="title" type="java.lang.String" required="true"%>
<jsp:doBody var="body" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="css" value="jquery-ui-atlas" />

<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Atlas - ${title}</title>
        <link href="<c:url value="/sistema/css/sistema.css"/>" rel="stylesheet" type="text/css" media="screen" />
        <link href="<c:url value="/sistema/css/blitzer/${css}.css"/>" rel="stylesheet" type="text/css" media="screen" />
        <link href="<c:url value="/sistema/css/jquery.ui.selectmenu.css"/>" rel="stylesheet" type="text/css" media="screen" />
        <link href="<c:url value="/sistema/css/default/menu.css"/>" rel="stylesheet" type="text/css" media="screen" />
        <script type="text/javascript" src="<c:url value="/sistema/js/jquery.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/sistema/js/jquery-ui.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/sistema/js/jquery.ui.selectmenu.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/sistema/js/jquery.datepicker.ptbr.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/sistema/js/jquery.validate.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/sistema/js/jquery.validate.additional-methods.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/sistema/js/jquery.validate.messages.pt-br.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/sistema/js/tiny_mce/jquery.tinymce.js"/>"></script>
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
        <script type="text/javascript" src="<c:url value="/sistema/js/gmaps.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/sistema/js/sistema.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/sistema/js/Estabelecimento/Estabelecimento.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/sistema/js/navigator.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/sistema/js/ajax.js"/>"></script>
        <c:if test="${javaScript != null}">
            <script type="text/javascript" src="<c:url value="/sistema/js/${javaScript}/${javaScript}.js"/>"></script>
        </c:if> 
        <script type="text/javascript" >
            var $j = jQuery.noConflict();
            $j(document).ready(function() 
            {
                Sistema.initialize();
                <c:if test="${javaScript != null}">${javaScript}.<c:out value="${initMethod}" default="initialize"/>();</c:if>
            });
        </script>
        <script type="text/javascript" >
            $j(function() {
                $j('textarea.tinymce').tinymce({
                    // Location of TinyMCE script
                    script_url : '<c:url value="/sistema/js/tiny_mce/tiny_mce.js"/>',

                    // General options
                    mode : "exact",
                    elements : "elm3",
                    theme : "advanced",
                    skin : "o2k7",
                    skin_variant : "silver",
                    language : 'pt',
                    //plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

                    // Theme options
                    //theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
                    //theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                    //theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
                    //theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
                    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,cut,copy,paste,pastetext,pasteword,|,undo,redo,|,link,unlink,",
                    theme_advanced_buttons2 : "",
                    theme_advanced_buttons3 : "",
                    theme_advanced_toolbar_location : "top",
                    theme_advanced_toolbar_align : "left",
                    theme_advanced_statusbar_location : "bottom",
                    //theme_advanced_resizing : true,

                    // Example content CSS (should be your site CSS)
                    content_css : "css/content.css",

                    // Drop lists for link/image/media/template dialogs
                    //template_external_list_url : "lists/template_list.js",
                    //external_link_list_url : "lists/link_list.js",
                    //external_image_list_url : "lists/image_list.js",
                    //media_external_list_url : "lists/media_list.js",

                    // Replace values for the template plugin
                    template_replace_values : {
                        username : "Some User",
                        staffid : "991234"
                    }//,
                    
                    /*setup: function(ed) {
                        this.textLimit = 100;

                        ed.onKeyUp.add(function(ed, e) {
                            var editorId = ed.editorId;
                            var preStrip = ed.getContent();

                            var strip = preStrip.replace(/(<([^>]+)>)/ig, "");
                            if (strip.length > this.textLimit) {
                                var txt = $j(this).data(editorId);
                                tinyMCE.execCommand('mceSetContent', false, txt);
                                tinymce.DOM.setHTML(tinymce.DOM.get(ed.id + '_path_row'), "Reached text limit of " + this.textLimit + " characters");
                                return false;
                            }
                            else {
                                var text = (this.textLimit - strip.length) + " Characters left"
                                tinymce.DOM.setHTML(tinymce.DOM.get(ed.id + '_path_row'), text);

                                $j(this).data(editorId, preStrip);
                            }
                        });

                    }*/
                });
            });
        </script>
    </head>
    <body>
        <div class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" align="center" style="margin: 16px; font-size: 20px; padding: 8px;"> 
            Atlas<c:if test='${userSession.membro.nome != null}'> | Bem Vindo, ${userSession.membro.nome}!</c:if>
            <!--<c:if test='${userSession.membro.nome != null}'><a id="exit" href="<c:url value="/sistema/logout"/>" style="margin-left: 200px;">Sair</a></c:if>-->
        </div>
        <div id="main" style="min-height: 400px; margin-bottom: 50px;">
            <div style="margin: auto; width: 90%;" class="ui-helper-reset">
                <c:if test="${menu}">
                    <%@include file="../../sistema/menu.jspf" %>
                </c:if>
                    ${body}
                <c:if test='${userSession.msg != ""}'>
                    <div style="margin: 20px 0; padding: .7em .7em; display: none;" class="ui-state-error ui-corner-all"> 
                        <p>
                            <span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-info"></span>
                            <strong>Atenção!</strong> ${userSession.msg}
                        </p>
                    </div>
                </c:if>
            </div>
            <c:if test="${not empty errors}">
                <div id="erros" style="margin: 20px auto auto; width: 900px; padding: 10px 20px;" class="ui-state-error ui-corner-all">
                    <ul>
                        <c:forEach items="${errors}" var="error">
                            <li>${error.message }</li>
                        </c:forEach>
                    </ul>
                </div>
            </c:if>
        </div>
        <div class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" align="center" style="font-size: 10px; bottom: 0px; padding: 6px; position: fixed; width: 97%; margin: 0pt 16px;"> 
            Todos os direitos reservados 
        </div>
    </body>
</html>