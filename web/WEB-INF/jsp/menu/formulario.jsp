<%--
    Document   : formulario
    Created on : 05/10/2011, 10:37:14
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" javaScript="Menu" title="Adicionar Menu">
    <ovc:containerForm titulo="Adicionar Menu" action="adiciona" >
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="nome">Nome:</label>
                </td>
                <td>
                    <input id="nome" type="text" name="menu.nome" value="${menu.nome}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="link">Link:</label>
                </td>
                <td>
                    <input id="link" type="text" name="menu.link" value="${menu.link}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="link">Pai:</label>
                </td>
                <td>
                    <select id="codPerfil" name="menu.pai.id" style="width: 200px" class="inputDefault">
                        <option value="">Item Raiz</option>
                        <c:forEach items="${itemMenuList}" var="menu">
                            <option value="${menu.id}">${menu.nome}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div class="toolbar">
                        <button type="reset" icon="ui-icon-cancel" >Cancelar</button>
                        <button type="submit" icon="ui-icon-plus" >Inserir</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
</ovc:main>
