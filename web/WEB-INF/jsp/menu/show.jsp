<%-- 
    Document   : lista
    Created on : 04/10/2011, 17:11:36
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<div class="menu">
    <ul id="nav" class="ui-state-default">
        <c:forEach items="${itemMenuList}" var="menu">
            <li>
                <a href="<c:url value="${menu.link}"/>">${menu.nome}</a>
            </li>
        </c:forEach>
        <c:if test='${login.usuario.nome != null}'>
            <li style="float: right">
                <a href="<c:url value="/sistema/logout"/>">Sair</a>
            </li>
        </c:if>
    </ul>
</div>