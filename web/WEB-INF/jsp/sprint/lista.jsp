<%-- 
    Document   : lista
    Created on : 04/10/2011, 17:11:36
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Lista de Sprints">
    <ovc:containerForm action="lista" titulo="Buscar Sprint">
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="nome">Numero:</label>
                </td> 
                <td>
                    <input id="nome" type="number" name="sprint.numero" value="${busca.numero}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="hidden" value="${page}" name="page" id="page" />
                    <input type="hidden" value="${pageSize}" name="pageSize" id="pageSize" />
                    <input type="hidden" value="${total}" name="total" id="total" />
                    <div class="toolbar">
                        <button type="submit" icon="ui-icon-search" >Buscar</button>
                    </div>
                </td>
                
            </tr>
        </table>
    </ovc:containerForm>
    <ovc:containerList lista="${sprintList}" titulo="Lista de Sprints" vazio="Nenhuma sprint encontrada!" page="${page}" total="${total}" pageSize="${pageSize}">
        <table cellspacing="0" cellpadding="0" style="width: 100%" class="data">            
            <thead>
                <tr>
                    <th>Numero</th>
                    <th>Inicio</th>
                    <th>Fim</th>
                    <th colspan="3">Ações</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${sprintList}" var="sprint">
                    <tr>
                        <td class="tdData">Sprint ${sprint.numero}</td>
                        <td class="tdData"><fmt:formatDate pattern="dd/MM/yyyy" value="${sprint.dataInicial}" /></td>
                        <td class="tdData"><fmt:formatDate pattern="dd/MM/yyyy" value="${sprint.dataFinal}" /></td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-person" href="${sprint.id}/membro">Escolher time</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-search" href="<c:url value="/projeto/${projeto.id}/sprint/${sprint.id}/historia/lista" />">Lista de Historia</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-pencil" href="edita/${sprint.id}">Editar</a>
                        </td>
                        <!--td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-trash" href="remove/${sprint.id}">Remover</a>
                        </td-->
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </ovc:containerList>
</ovc:main>