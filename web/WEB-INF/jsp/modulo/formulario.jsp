<%--
    Document   : formulario
    Created on : 05/10/2011, 10:37:14
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" javaScript="Modulo" title="Adicionar Modulo">
    <ovc:containerForm titulo="Adicionar Modulo" action="adiciona" >
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="nome">Nome:</label>
                </td>
                <td>
                    <input id="nome" type="text" name="modulo.nome" value="${modulo.nome}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="descricao">Descrição:</label>
                </td>
                <td>
                    <input id="descricao" type="text" name="modulo.descricao" value="${modulo.descricao}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="comentario">Comentário:</label>
                </td>
                <td>
                    <textarea id="comentario" name="modulo.comentario" style="width: 538px; height: 200px;" class="tinymce">
                        ${modulo.comentario}
                    </textarea>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="Projeto">Projeto:</label>
                </td> 
                <td>
                    <select id="projetoId" name="modulo.projeto.id" style="width: 200px" class="inputDefault">
                        <option value="">Selecione Projeto</option>
                        <c:forEach items="${projetoList}" var="projeto">
                            <option value="${projeto.id}" >${projeto.nome}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="nota">Nota:</label>
                </td> 
                <td>
                    <select id="nota" name="modulo.nota" style="width: 200px" class="inputDefault">
                        <option value="">Selecione Nota</option>
                        <c:forEach items="${notaList}" var="nota">
                            <option value="${nota}" >${nota.descricao}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div class="toolbar">
                        <button type="reset" icon="ui-icon-cancel" >Cancelar</button>
                        <button type="submit" icon="ui-icon-plus" >Inserir</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
</ovc:main>
