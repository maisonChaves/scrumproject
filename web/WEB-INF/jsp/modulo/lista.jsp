<%-- 
    Document   : lista
    Created on : 04/10/2011, 17:11:36
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Lista de Modulos">
    <ovc:containerList lista="${moduloList}" titulo="Lista de Modulo" vazio="Nenhum modulo encontrado!" page="${page}" total="${total}" pageSize="${pageSize}">
        <table cellspacing="0" cellpadding="0" style="width: 100%" class="data">            
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Nota</th>
                    <th>Projeto</th>
                    <th colspan="2">Ações</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${moduloList}" var="modulo">
                    <tr>
                        <td class="tdData">${modulo.nome}</td>
                        <td class="tdData">${modulo.descricao}</td>
                        <td class="tdData">${modulo.nota.descricao}</td>
                        <td class="tdData">${modulo.projeto.nome}</td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-pencil" href="edita?id=${modulo.id}">Editar</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-trash" href="remove?id=${modulo.id}">Remover</a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </ovc:containerList>
</ovc:main>