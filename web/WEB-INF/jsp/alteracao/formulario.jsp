<%--
    Document   : formulario
    Created on : 05/10/2011, 10:37:14
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" javaScript="Alteracao" title="Adicionar Altera��o">
    <ovc:containerForm titulo="Adicionar Altera��o" action="adiciona" >
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="nome">Tabela / Arquivo:</label>
                </td>
                <td>
                    <input id="nome" type="text" name="alteracao.nome" value="${alteracao.nome}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="horas">Local / Pasta / Banco:</label>
                </td>
                <td>
                    <input id="horas" type="text" name="alteracao.referencia" value="${alteracao.referencia}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="tipoAlteracao">Tipo:</label>
                </td> 
                <td>
                    <select id="tipoAlteracao" name="alteracao.tipo" style="width: 200px" class="inputDefault">
                        <c:forEach items="${tipoAlteracaoList}" var="tipo">
                            <option value="${tipo}" <c:if test="${tipo == alteracao.tipo}">selected</c:if> >${tipo.descricao}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label>Controle:</label>
                </td> 
                <td align="left">
                    <input <c:if test="${alteracao.versionado}">checked</c:if> type="checkbox" class="checkDefault" id="versionado" name="alteracao.versionado" value="true"/>
                    <label for="versionado">
                        Versionado
                    </label>
                    <input <c:if test="${alteracao.maquinaTeste}">checked</c:if> type="checkbox" class="checkDefault" id="maquinaTeste" name="alteracao.maquinaTeste" value="true"/>
                    <label for="maquinaTeste">
                        Maquina de Teste
                    </label>
                </td> 
            </tr>
            <tr>
                <td align="right">
                    <label for="observacao">Descri��o:</label>
                </td>
                <td>
                    <textarea id="observacao" name="alteracao.observacao" style="width: 538px; height: 200px;" class="tinymce">
                        ${alteracao.observacao}
                    </textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="hidden" name="alteracao.tarefa.id" value="${tarefa.id}" />
                    <div class="toolbar">
                        <button type="reset" icon="ui-icon-cancel" >Cancelar</button>
                        <button type="submit" icon="ui-icon-plus" >Inserir</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
</ovc:main>
