<%--
    Document   : edita
    Created on : 05/10/2011, 11:20:04
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Editar Projeto">
    <ovc:containerForm titulo="Editar Projeto" action="altera">
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="nome">Descrição:</label>
                </td>
                <td>
                    <input id="descricao" type="text" name="itemRetrospectiva.descricao" value="${itemRetrospectiva.descricao}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="perfil">Tipo:</label>
                </td> 
                <td>
                    <select id="tipo" name="itemRetrospectiva.tipo" style="width: 200px" class="inputDefault">
                        <option value="">Selecione Tipo</option>
                        <c:forEach items="${tipoItemRetrospectivaList}" var="tipo">
                            <option value="${tipo}" <c:if test="${tipo == itemRetrospectiva.tipo}">selected</c:if> >${tipo.descricao}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="hidden" name="itemRetrospectiva.id" value="${itemRetrospectiva.id}" />
                    <div class="toolbar">
                        <button icon="ui-icon-pencil" type="submit">Alterar</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
    <script type="text/javascript">
        $j(document).ready(function()
        {
            $j('.containerForm form').validate({
                //errorClass: 'ui-state-error',
                rules: {
                    "itemRetrospectiva.descricao": {
                        required: true,
                        minlength: 3
                    }
                }
            });
        });
    </script>
</ovc:main>