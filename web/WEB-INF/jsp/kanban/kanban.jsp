<%-- 
    Document   : index
    Created on : 12/03/2013, 09:54:34
    Author     : Maison
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<ovc:main menu="${true}" title="Kanban - ${sprint.projeto.nome} - Sprint ${sprint.numero}">
    <ovc:container nome="kanbanContainer" titulo="${sprint.projeto.nome} - Sprint ${sprint.numero}" toggle="${false}">
        <table>
            <tr>
                <th>Historia</th>
                <th>Nova</th>
                <th>Em Andamento</th>
                <th>A Verificar</th>
                <th>Feito</th>
            </tr>
            <c:forEach items="${sprint.historias}" var="historia">
                <tr>
                    <td valign="top">
                        <div class="portlet historia">
                            <div class="portlet-header">${historia.id}</div>
                            <div class="portlet-content">${historia.titulo}</div>
                        </div>
                    </td>
                    <td valign="top">
                        <div class="column" statusTarefa="N" historia="${historia.id}">
                            <c:forEach items="${historia.tarefas}" var="tarefa">
                                <c:if test='${tarefa.status == "N"}'>
                                    <div class="portlet" codigo="${tarefa.id}">
                                        <div class="portlet-header">${tarefa.id}</div>
                                        <div class="portlet-content">${tarefa.descricao}</div>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                    </td>
                    <td valign="top">
                        <div class="column" statusTarefa="A" historia="${historia.id}">
                            <c:forEach items="${historia.tarefas}" var="tarefa">
                                <c:if test='${tarefa.status == "A"}'>
                                    <div class="portlet" codigo="${tarefa.id}">
                                        <div class="portlet-header">${tarefa.id}</div>
                                        <div class="portlet-content">${tarefa.descricao}</div>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                    </td>
                    <td valign="top">
                        <div class="column" statusTarefa="V" historia="${historia.id}">
                            <c:forEach items="${historia.tarefas}" var="tarefa">
                                <c:if test='${tarefa.status == "V"}'>
                                    <div class="portlet" codigo="${tarefa.id}">
                                        <div class="portlet-header">${tarefa.id}</div>
                                        <div class="portlet-content">${tarefa.descricao}</div>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                    </td>
                    <td valign="top">
                        <div class="column" statusTarefa="F" historia="${historia.id}">
                            <c:forEach items="${historia.tarefas}" var="tarefa">
                                <c:if test='${tarefa.status == "F"}'>
                                    <div class="portlet" codigo="${tarefa.id}">
                                        <div class="portlet-header">${tarefa.id}</div>
                                        <div class="portlet-content">${tarefa.descricao}</div>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </ovc:container>

    <style>
        body { min-width: 520px; }
        .column { width: 170px; float: left; min-height: 200px }
        .historia { width: 170px; }
        .portlet { margin: 0 0 1em 0; }
        .portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; }
        .portlet-header .ui-icon { float: right; }
        .portlet-content { padding: 0.4em; }
        .ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
        .ui-sortable-placeholder * { visibility: hidden; }
    </style>

    <script type="text/javascript">
        $j(function() {
            $j(".column").sortable({
                connectWith: ".column",
                containment: ".kanbanContainer .containerBody"
            });

            $j(".column").droppable({
                drop: function(event, ui) {
                    var status = $j(event.target).attr("statusTarefa");
                    var historia = $j(event.target).attr("historia");
                    var codigo = ui.draggable.attr("codigo");

                    changeStatusTarefa(status, codigo, historia);
                }
            });

            $j(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
                    .find(".portlet-header")
                    .addClass("ui-widget-header ui-corner-all")
                    .prepend("<span class='ui-icon ui-icon-minusthick'></span>")
                    .end()
                    .find(".portlet-content");

            $j(".portlet-header .ui-icon").click(function() {
                $j(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
                $j(this).parents(".portlet:first").find(".portlet-content").toggle();
            });

            $j(".column").disableSelection();
        });

        function changeStatusTarefa(status, codigo, historia)
        {
            Ajax.execute({
                url: "changeStatusTarefa",
                data: {
                    "tarefa.status"     : status, 
                    "tarefa.id"         : codigo, 
                    "tarefa.historia.id"   : historia},
                beforeSend: function() {
                    $j("#imagem").html(Ajax.loadingImg);
                },
                success: function(retorno) {
                    if (retorno.string === "true")
                    {
                        $j("#imagem").html(Ajax.confirmImg);
                    }
                    else if (retorno.string === "false")
                    {
                        $j("#imagem").html(Ajax.errorImg);
                    }
                }

            });
        }

    </script>
</ovc:main>
