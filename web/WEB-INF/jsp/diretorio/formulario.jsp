<%--
    Document   : formulario
    Created on : 05/10/2011, 10:37:14
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" javaScript="Diretorio" title="Adicionar Diretorio">
    <ovc:containerForm titulo="Adicionar Diretorio" action="adiciona" >
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="nome">Nome:</label>
                </td>
                <td>
                    <input id="nome" type="text" name="diretorio.nome" value="${diretorio.nome}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="tipo">Tipo:</label>
                </td> 
                <td>
                    <select id="nota" name="diretorio.tipo" style="width: 200px" class="inputDefault">
                        <option value="">Selecione Tipo</option>
                        <c:forEach items="${tipoList}" var="tipo">
                            <option value="${tipo}" >${tipo.descricao}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label>Modulo:</label>
                </td>
                <td>
                    <c:forEach items="${moduloList}" var="modulo" varStatus="i">
                        <c:set var="checked" value='${""}' />

                        <c:forEach items="${diretorio.modulos}" var="m">
                            <c:if test="${m.id == modulo.id}">
                                <c:set var="checked" value='${"checked"}' />
                            </c:if>
                        </c:forEach>

                        <input ${checked} type="checkbox" class="checkDefault" id="modulo${modulo.id}" name="modulos[${modulo.id}]" value="${modulo.id}"/>
                        <label for="modulo${modulo.id}">
                            ${modulo.nome}
                        </label>
                    </c:forEach>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div class="toolbar">
                        <button type="reset" icon="ui-icon-cancel" >Cancelar</button>
                        <button type="submit" icon="ui-icon-plus" >Inserir</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
</ovc:main>
