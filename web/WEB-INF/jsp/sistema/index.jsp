<%-- 
    Document   : index
    Created on : 12/03/2013, 09:54:34
    Author     : Maison
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<ovc:main menu="${true}" title="Home">
    <c:forEach items="${projetoList}" var="projeto">
        <div class="space"></div>
        <ovc:container nome="projeto${projeto.id}" titulo="${projeto.nome}">

            <div align="center" class="toolbar">
                <button type="button" icon="ui-icon-search" >Lista de Historias</button>
                <button type="button" icon="ui-icon-plusthick" >Nova Historia</button>
                <button type="button" icon="ui-icon-search" >Lista de Sprints</button>
                <button type="button" icon="ui-icon-plusthick" >Nova Sprint</button>
            </div>
            <div class="space"></div>
            <div class="sprintTab">
                <ul class="ulSprint ulNoListStyle">
                    <li>
                        <a href="#backlog${projeto.id}">Backlog</a>
                    </li>
                    <c:forEach items="${projeto.sprints}" var="sprint">
                        <li>
                            <a href="#sprint${sprint.id}">Sprint ${sprint.id}</a>
                        </li>
                    </c:forEach>
                </ul>
                    
                    <div id="backlog${projeto.id}">

                        <ul class="ulHistoria ulNoListStyle">
                            <c:forEach items="${projeto.historias}" var="historia">
                                <li class="ui-corner-all ui-state-default">
                                    ${historia.titulo}
                                </li>
                            </c:forEach>
                        </ul>

                        <div align="center" class="toolbar">
                            <button type="button" icon="ui-icon-search" >Lista de Historias</button>
                            <button type="button" icon="ui-icon-plusthick" >Nova Historia</button>
                        </div>
                    </div>
                    
                <c:forEach items="${projeto.sprints}" var="sprint">
                    <div id="sprint${sprint.id}">

                        <ul class="ulHistoria ulNoListStyle">
                            <c:forEach items="${sprint.historias}" var="historia">
                                <li class="ui-corner-all ui-state-default">
                                    ${historia.titulo}
                                </li>
                            </c:forEach>
                        </ul>

                        <div align="center" class="toolbar">
                            <button type="button" icon="ui-icon-search" >Lista de Historias</button>
                            <button type="button" icon="ui-icon-plusthick" >Nova Historia</button>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </ovc:container>
    </c:forEach>
    <style type="text/css">
        .ulNoListStyle
        {
            list-style: none;
        }
        .ulHistoria li
        {
            padding: 5px;
            margin: 3px 0px;
        }
        .space
        {
            margin-top: 10px;
        }
    </style>
    <script type="text/javascript">
        $j(".sprintTab").tabs({heightStyle: "auto"});
    </script>
</ovc:main>
