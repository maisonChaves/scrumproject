<%--
    Document   : loginForm
    Created on : 25/10/2011, 10:08:33
    Author     : Maison Chaves
--%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${false}" title="Login">
    <style type="text/css">
        .loginForm{
            width       : 450px;
            padding-top : 100px;
        }
    </style>
    <ovc:container titulo="Login" nome="containerForm loginForm" toggle="false">
        <form action="login" method="POST">
            <table style="width: 100%">
                <tr>
                    <td align="right">
                        <label for="login">Login:</label>
                    </td>
                    <td>
                        <input id="login" type="text" name="login.login" style="width: 300px" class="inputDefault"/>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <label for="senha">Senha:</label>
                    </td>
                    <td>
                        <input id="senha" type="password" name="login.senha" style="width: 300px" class="inputDefault"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <div class="toolbar">
                            <button icon="ui-icon-key" type="submit">Login</button>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
        <div style="margin: 20px 0; padding: .7em .7em;" class="ui-state-error ui-corner-all"> 
            <p>
                <span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-info"></span>
                <strong>Atenção!</strong> A senha padrão é 'senha' sem as aspas.
            </p>
        </div>
    </ovc:container>
</ovc:main>
