<%--
    Document   : formulario
    Created on : 05/10/2011, 10:37:14
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" javaScript="Membro" title="Adicionar Membro">
    <ovc:containerForm titulo="Adicionar Membro" action="adiciona" >
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="nome">Nome:</label>
                </td>
                <td>
                    <input id="nome" type="text" name="membro.nome" value="${membro.nome}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="nome">Horas:</label>
                </td>
                <td>
                    <input id="horas" type="text" name="membro.horas" value="${membro.horas}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="funcao">Papel:</label>
                </td> 
                <td>
                    <select id="funcao" name="membro.funcao" style="width: 200px" class="inputDefault">
                        <option value="">Selecione Funcao</option>
                        <c:forEach items="${funcaoList}" var="funcao">
                            <option value="${funcao}" <c:if test="${funcao == membro.funcao}">selected</c:if> >${funcao.descricao}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="login">Login:</label>
                </td>
                <td>
                    <input id="login" type="text" name="membro.login.login" value="${membro.login.login}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="senha">Senha:</label>
                </td>
                <td>
                    <input id="senha" type="password" name="membro.login.senha" value="${membro.senha}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="confirmacao">Confirmação:</label>
                </td>
                <td>
                    <input id="confirmacao" type="password" name="confirmacao" value="${confirmacao}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div class="toolbar">
                        <button type="reset" icon="ui-icon-cancel" >Cancelar</button>
                        <button type="submit" icon="ui-icon-plus" >Inserir</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
</ovc:main>
