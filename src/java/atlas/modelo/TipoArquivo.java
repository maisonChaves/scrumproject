
package atlas.modelo;

/**
 *
 * @author Maison
 */
public enum TipoArquivo {
    JSP("jsp"), JAVA("java"), JS("js"), TABELA("Tabela");
    
    private String descricao;

    private TipoArquivo(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
