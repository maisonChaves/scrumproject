package atlas.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Maison Chaves
 */
@Entity
public class Projeto implements Serializable
{
    @OneToMany(mappedBy = "projeto")
    private List<Modulo> modulos;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    @OneToMany(mappedBy = "projeto")
    private List<Historia> historias;
    @OneToMany(mappedBy = "projeto")
    //@Fetch(FetchMode.JOIN)
    private List<Sprint> sprints;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public List<Sprint> getSprints()
    {
        Integer start = sprints.size() < 15 ? 0 : sprints.size()-15; 
        return sprints.subList(start, sprints.size());
    }

    public void setSprints(List<Sprint> sprints)
    {
        this.sprints = sprints;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public List<Historia> getHistorias()
    {
        return historias;
    }

    public void setHistorias(List<Historia> historias)
    {
        this.historias = historias;
    }

    public List<Modulo> getModulos() {
        return modulos;
    }

    public void setModulos(List<Modulo> modulos) {
        this.modulos = modulos;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Projeto))
        {
            return false;
        }
        Projeto other = (Projeto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "atlas.modelo.Projeto[ id=" + id + " ]";
    }
}
