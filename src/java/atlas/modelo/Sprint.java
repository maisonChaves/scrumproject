package atlas.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author Maison Chaves
 */
@Entity
public class Sprint implements Serializable
{
    private static final long serialVersionUID = 1L;
    private Long id;
    private Integer numero;
    private Date dataInicial;
    private Date dataFinal;
    private String meta;
    private String pronto;
    private Projeto projeto;
    private List<Historia> historias;
    private Set<Membro> membros;
    private List<ItemRetrospectiva> itemRetrospectivas;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getNumero()
    {
        return numero;
    }

    public void setNumero(Integer numero)
    {
        this.numero = numero;
    }

    @Temporal(TemporalType.DATE)
    public Date getDataInicial()
    {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial)
    {
        this.dataInicial = dataInicial;
    }

    @Temporal(TemporalType.DATE)
    public Date getDataFinal()
    {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal)
    {
        this.dataFinal = dataFinal;
    }

    public String getMeta()
    {
        return meta;
    }

    public void setMeta(String meta)
    {
        this.meta = meta;
    }

    public String getPronto()
    {
        return pronto;
    }

    public void setPronto(String pronto)
    {
        this.pronto = pronto;
    }

    @ManyToOne
    @JoinColumn(name = "projetoId")
    public Projeto getProjeto()
    {
        return projeto;
    }

    public void setProjeto(Projeto projeto)
    {
        this.projeto = projeto;
    }

    @OneToMany(mappedBy = "sprint")
    public List<ItemRetrospectiva> getItemRetrospectivas()
    {
        return itemRetrospectivas;
    }

    public void setItemRetrospectivas(List<ItemRetrospectiva> itemRetrospectivas)
    {
        this.itemRetrospectivas = itemRetrospectivas;
    }

    @ManyToMany(targetEntity = Membro.class)
    @JoinTable(name = "sprintMembro",
            joinColumns =
            @JoinColumn(name = "sprintId"),
            inverseJoinColumns =
            @JoinColumn(name = "membroId"))
    public Set<Membro> getMembros()
    {
        return membros;
    }

    public void setMembros(Set<Membro> membros)
    {
        this.membros = membros;
    }

    @OneToMany(mappedBy = "sprint")
    public List<Historia> getHistorias()
    {
        return historias;
    }

    public void setHistorias(List<Historia> historias)
    {
        this.historias = historias;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sprint))
        {
            return false;
        }
        Sprint other = (Sprint) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "atlas.modelo.Sprint[ id=" + id + " ]";
    }

    @Transient
    public List<Alteracao> getAlteracoes()
    {
        List<Alteracao> listAlteracoes = new ArrayList<Alteracao>();
        if (historias != null)
        {
            for (Historia h : historias)
            {
                if (h.getAlteracoes() != null)
                {
                    listAlteracoes.addAll(h.getAlteracoes());
                }
            }
        }

        return listAlteracoes;
    }
}
