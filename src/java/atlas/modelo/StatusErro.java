
package atlas.modelo;

/**
 *
 * @author Maison
 */
public enum StatusErro
{
    N("Novo"), A("Atribuido"), R("Resolvido"), F("Fechado");
    private String descricao;

    private StatusErro(String descricao)
    {
        this.descricao = descricao;
    }

    public String getDescricao()
    {
        return descricao;
    }
}
