
package atlas.modelo;

/**
 *
 * @author Maison
 */
public enum TipoPacote {
    W("Web"), J("Java"), B("Banco");
    private String descricao;

    private TipoPacote(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
