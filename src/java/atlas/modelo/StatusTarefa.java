
package atlas.modelo;

/**
 *
 * @author Maison Chaves
 */
public enum StatusTarefa
{
    N("Nova"), A("Em andamento"), V("À verificar"), F("Feito"), E("Entregue");

    private final String descricao;

    private StatusTarefa(String descricao)
    {
        this.descricao = descricao;
    }

    public String getDescricao()
    {
        return descricao;
    }
    
    public boolean isStatus(StatusTarefa status)
    {
        for (StatusTarefa tipoEnvio : StatusTarefa.values()) {
            if (tipoEnvio.equals(status)) {
                return true;
            }
        }
        return false;
    }
    public static StatusTarefa findStatus(String status) {
        for (StatusTarefa tipoEnvio : StatusTarefa.values()) {
            if (tipoEnvio.getDescricao().equals(status)) {
                return tipoEnvio;
            }
        }
        throw new IllegalArgumentException("Status não encontrado!");
    }
}
