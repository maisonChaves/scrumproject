package atlas.modelo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Maison Chaves
 */
@Entity
public class Tarefa implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Enumerated(EnumType.STRING)
    private TipoTarefa tipo;
    @Enumerated(EnumType.STRING)
    private StatusTarefa status;
    private String descricao;
    private Float horas;
    private Float horasGasta;
    @Temporal(TemporalType.DATE)
    private Calendar dataEncerramento; 
    @ManyToOne
    @JoinColumn(name = "historiaId")
    private Historia historia;
    @ManyToOne
    @JoinColumn(name = "membroId")
    private Membro membro;
    @OneToMany(mappedBy = "tarefa")
    private List<Alteracao> alteracoes;
    private Integer idExterno;

    public Integer getIdExterno() {
        return idExterno;
    }

    public void setIdExterno(Integer idExterno) {
        this.idExterno = idExterno;
    }
    
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public TipoTarefa getTipo()
    {
        return tipo;
    }

    public void setTipo(TipoTarefa tipo)
    {
        this.tipo = tipo;
    }

    public StatusTarefa getStatus()
    {
        return status;
    }

    public void setStatus(StatusTarefa status)
    {
        this.status = status;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    public Float getHoras()
    {
        return horas;
    }

    public void setHoras(Float horas)
    {
        this.horas = horas;
    }

    public Float getHorasGasta()
    {
        return horasGasta;
    }

    public void setHorasGasta(Float horasGasta)
    {
        this.horasGasta = horasGasta;
    }

    public Historia getHistoria()
    {
        return historia;
    }

    public void setHistoria(Historia historia)
    {
        this.historia = historia;
    }

    public Membro getMembro()
    {
        return membro;
    }

    public void setMembro(Membro membro)
    {
        this.membro = membro;
    }

    public List<Alteracao> getAlteracoes()
    {
        return alteracoes;
    }

    public void setAlteracoes(List<Alteracao> alteracoes)
    {
        this.alteracoes = alteracoes;
    }

    public Calendar getDataEncerramento() {
        return dataEncerramento;
    }

    public void setDataEncerramento(Calendar dataEncerramento) {
        this.dataEncerramento = dataEncerramento;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tarefa))
        {
            return false;
        }
        Tarefa other = (Tarefa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "atlas.modelo.Tarefa[ id=" + id + " ]";
    }
}
