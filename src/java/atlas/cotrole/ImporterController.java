package atlas.cotrole;

import atlas.importer.ImporterRedMine;
import br.com.caelum.vraptor.Resource;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author Maison Chaves
 */
@Resource
public class ImporterController {
    private final ImporterRedMine importerRedMine;


    /**
     *
     * @param importerRedMine
     */
    public ImporterController(ImporterRedMine importerRedMine) {
        this.importerRedMine = importerRedMine;
    }

    /**
     *
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
    public void importar() throws FileNotFoundException, IOException {
        importerRedMine.importar();
    }

    
}
