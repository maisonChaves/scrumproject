
package atlas.cotrole;
import atlas.dao.ModuloDAO;
import atlas.dao.ProjetoDAO;
import atlas.modelo.Modulo;
import atlas.modelo.Nota;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import java.util.Arrays;
import java.util.List;

/**
 * @author Maison Chaves
 */
@Resource
public class ModuloController
{
    private final ModuloDAO dao;
    private final Result result;
    private final Validator validator;
    private final ProjetoDAO projetoDAO;

    /**
     *
     * @param moduloDAO
     * @param result
     * @param validator
     */
    public ModuloController(ModuloDAO moduloDAO, ProjetoDAO projetoDAO, Result result, Validator validator)
    {
        this.validator = validator;
        this.result = result;
        this.dao = moduloDAO;
        this.projetoDAO = projetoDAO;
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Modulo> lista(Integer page, Integer pageSize)
    {
        if (page == null || pageSize == null)
        {
            page = 1;
            pageSize = 10;
        }

        Integer total = dao.getTotalRegistros();

        result.include("page", page);
        result.include("pageSize", pageSize);
        result.include("total", total);
        result.include("totalPage", ((total - 1) / pageSize) + 1);
        return dao.busca(page, pageSize);
    }

    /**
     *
     * @param modulo
     */
    public void adiciona(final Modulo modulo)
    {
        if (modulo.getNome() == null || modulo.getNome().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).formulario();

        dao.salva(modulo);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     * @return
     */
    public Modulo edita(Long id)
    {
        result.include("projetoList", projetoDAO.listaTudo());
        result.include("notaList", Arrays.asList(Nota.values()));
        return dao.carrega(id);
    }

    /**
     *
     * @param modulo
     */
    public void altera(Modulo modulo)
    {
        if (modulo.getNome()== null || modulo.getNome().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).edita(modulo.getId());

        dao.altera(modulo);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     */
    public void remove(Long id)
    {
        Modulo modulo = dao.carrega(id);
        dao.remove(modulo);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     */
    public void formulario()
    {
        result.include("projetoList", projetoDAO.listaTudo());
        result.include("notaList", Arrays.asList(Nota.values()));
    }
}
