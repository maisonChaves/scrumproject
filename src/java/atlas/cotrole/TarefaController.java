package atlas.cotrole;

import atlas.dao.TarefaDAO;
import atlas.dao.HistoriaDAO;
import atlas.modelo.Tarefa;
import atlas.modelo.Historia;
import atlas.modelo.StatusTarefa;
import atlas.modelo.TipoTarefa;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * @author Maison Chaves
 */
@Resource
public class TarefaController
{
    private final TarefaDAO dao;
    private final Result result;
    private final Validator validator;
    private final HistoriaDAO historiaDAO;

    /**
     *
     * @param tarefaDAO
     * @param result
     * @param validator
     */
    public TarefaController(HistoriaDAO historiaDAO, TarefaDAO tarefaDAO, Result result, Validator validator)
    {
        this.validator = validator;
        this.result = result;
        this.dao = tarefaDAO;
        this.historiaDAO = historiaDAO;
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Path("/historia/{historia.id}/tarefa/lista")
    public List<Tarefa> lista(Historia historia/*, Integer page, Integer pageSize*/)
    {
        /*if (page == null || pageSize == null)
        {
            page = 1;
            pageSize = 10;
        }*/

        Tarefa tarefa = new Tarefa();
        tarefa.setHistoria(historia);

        Integer total = dao.getTotalRegistros(tarefa);

        result.include("page", 1);
        result.include("pageSize", total);
        result.include("total", total);
        result.include("totalPage", ((total - 1) / total) + 1);
        return dao.busca(tarefa);
    }

    /**
     *
     * @param tarefa
     */
    @Path("/historia/{historia.id}/tarefa/adiciona")
    public void adiciona(Historia historia, final Tarefa tarefa)
    {
        if (tarefa.getDescricao()== null || tarefa.getDescricao().length() < 3)
        {
            validator.add(new ValidationMessage("Titulo é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).formulario(historia);

        if (tarefa.getMembro().getId() == null)
        {
            tarefa.setMembro(null);
        }
        dao.salva(tarefa);
        result.redirectTo(this).lista(historia);
    }

    /**
     *
     * @param id
     * @return
     */
    @Path("/historia/{historia.id}/tarefa/edita/{id}")
    public Tarefa edita(Historia historia, Long id)
    {
        Historia historiaAtual = historiaDAO.carrega(historia.getId());
        result.include("membroList", historiaAtual.getSprint().getMembros());
        
        result.include("tipoTarefaList", Arrays.asList(TipoTarefa.values()));
        result.include("statusTarefaList", Arrays.asList(StatusTarefa.values()));
        return dao.carrega(id);
    }

    /**
     *
     * @param tarefa
     */
    @Path("/historia/{historia.id}/tarefa/altera")
    public void altera(Historia historia, Tarefa tarefa)
    {
        if (tarefa.getDescricao()== null || tarefa.getDescricao().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).edita(historia, tarefa.getId());

        if (tarefa.getMembro().getId() == null)
        {
            tarefa.setMembro(null);
        }
        if(tarefa.getStatus().equals(StatusTarefa.F))
        {
            tarefa.setDataEncerramento(Calendar.getInstance());
        }
        
        dao.altera(tarefa);
        result.redirectTo(this).lista(historia);
    }

    /**
     *
     * @param id
     */
    @Path("/historia/{historia.id}/tarefa/remove/{id}")
    public void remove(Historia historia, Long id)
    {
        Tarefa tarefa = dao.carrega(id);
        dao.remove(tarefa);
        result.redirectTo(this).lista(historia);
    }

    @Path("/historia/{historia.id}/tarefa/formulario")
    public Historia formulario(Historia historia)
    {
        Historia historiaAtual = historiaDAO.carrega(historia.getId());
        result.include("membroList", historiaAtual.getSprint().getMembros());
        
        result.include("tipoTarefaList", Arrays.asList(TipoTarefa.values()));
        result.include("statusTarefaList", Arrays.asList(StatusTarefa.values()));
        return historia;
    }
}
