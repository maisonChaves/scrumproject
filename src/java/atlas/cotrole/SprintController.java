
package atlas.cotrole;
import atlas.dao.MembroDAO;
import atlas.dao.SprintDAO;
import atlas.modelo.Membro;
import atlas.modelo.Projeto;
import atlas.modelo.Sprint;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import java.util.HashSet;
import java.util.List;

/**
 * @author Maison Chaves
 */
@Resource
public class SprintController
{
    private final SprintDAO dao;
    private final Result result;
    private final Validator validator;
    private final MembroDAO membroDAO;

    /**
     *
     * @param sprintDAO
     * @param result
     * @param validator
     */
    public SprintController(SprintDAO sprintDAO, MembroDAO membroDAO, Result result, Validator validator)
    {
        this.validator = validator;
        this.result = result;
        this.dao = sprintDAO;
        this.membroDAO = membroDAO;
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Path("/projeto/{projeto.id}/sprint/lista") 
    public List<Sprint> lista(Projeto projeto, Sprint sprint, Integer page, Integer pageSize)
    {
        if (page == null || pageSize == null)
        {
            page = 1;
            pageSize = 10;
        }

        sprint.setProjeto(projeto);

        Integer total = dao.getTotalRegistros(sprint);
        
        Integer totalPage = ((total - 1) / pageSize) + 1;
        page = page > totalPage ? 1 : page;
        
        result.include("page", page);
        result.include("pageSize", pageSize);
        result.include("total", total);
        result.include("totalPage", totalPage);
        result.include("projeto", projeto);
        result.include("busca", sprint);
        return dao.busca(sprint, page, pageSize);
    }
    
    /**
     *
     * @param sprint
     */
    @Path("/projeto/{projeto.id}/sprint/adiciona") 
    public void adiciona(Projeto projeto, final Sprint sprint)
    {
        if (sprint.getNumero() == null)
        {
            validator.add(new ValidationMessage("Numero é obrigatório", "produto.nome"));
        }
        if (!dao.check(sprint))
        {
            validator.add(new ValidationMessage("Essa sprint já se encontra cadastrada", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).formulario(projeto);

        dao.salva(sprint);
        result.redirectTo(this).lista(projeto,null, null, null);
    }

    /**
     *
     * @param id
     * @return
     */
    @Path("/projeto/{projeto.id}/sprint/edita/{id}")
    public Sprint edita(Long id)
    {
        return dao.carrega(id);
    }

    /**
     *
     * @param sprint
     */
    @Path("/projeto/{projeto.id}/sprint/altera")
    public void altera(Projeto projeto, Sprint sprint)
    {
        if (sprint.getNumero() == null)
        {
            validator.add(new ValidationMessage("Nome é obrigatório", "produto.nome"));
        }
        if (!dao.check(sprint))
        {
            validator.add(new ValidationMessage("Essa sprint já se encontra cadastrada", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).edita(sprint.getId());

        dao.altera(sprint);
        result.redirectTo(this).lista(projeto,null, null, null);
    }

    /**
     *
     * @param id
     */
    @Path("/projeto/{projeto.id}/sprint/remove/{id}")
    public void remove(Projeto projeto, Long id)
    {
        Sprint sprint = dao.carrega(id);
        dao.remove(sprint);
        result.redirectTo(this).lista(projeto,null,null,null);
    }

    /**
     *
     */
    @Path("/projeto/{projeto.id}/sprint/formulario") 
    public Projeto formulario(Projeto projeto)
    {
        return projeto;
    }
    
    /**
     *
     */
    @Path("/projeto/{projeto.id}/sprint/{sprint.id}/membro") 
    public Sprint membro(Projeto projeto, Sprint sprint)
    {
        result.include("projeto", projeto);
        result.include("membroList", membroDAO.listaTudo());
        return dao.carrega(sprint.getId());
    }
    @Path("/projeto/{projeto.id}/sprint/{sprint.id}/membro/seleciona") 
    public void seleciona(Projeto projeto, Sprint sprint, List<Long> membros)
    {
        Sprint sprintMembro = dao.carrega(sprint.getId());
        sprintMembro.setMembros(new HashSet<Membro>());
        
        for (Long idMembro : membros)
        {
            Membro m = new Membro();
            m.setId(idMembro);
            sprintMembro.getMembros().add(m);
        }
        
        dao.salva(sprintMembro);
        result.redirectTo(this).lista(projeto,null, null, null);
    }
}
