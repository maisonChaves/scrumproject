package atlas.infra;

import atlas.modelo.Membro;
import atlas.modelo.Projeto;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;
import java.util.List;

/**
 * Objeto que centraliza os dados da sessão
 *
 * @author Maison
 */
@Component
@SessionScoped
public class UserSession
{
    private Membro membro;
    private String msg;
    private List<Projeto> projetos;

    /**
     *
     * @return
     */
    public Membro getMembro()
    {
        return membro;
    }

    /**
     *
     * @return
     */
    public String getMsg()
    {
        return msg;
    }

    public List<Projeto> getProjetos() {
        return projetos;
    }
    
    /**
     *
     * @param msg
     */
    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    /**
     * Salva os dados necessarios para a sessão
     *
     * @param usuario dados do usuario logado
     * @param list lista de telas que o usuario tem acesso
     */
    public void logar(Membro membro, List<Projeto> projetos)
    {
        this.membro = membro;
        this.projetos = projetos;
    }

    /**
     * Remove os dados de login da sessão
     */
    public void logout()
    {
        this.membro = null;
        this.projetos = null;
    }

    /**
     * verifica se o usuario esta logado
     *
     * @return
     */
    public boolean isLogado()
    {
        return membro != null;
    }
}
