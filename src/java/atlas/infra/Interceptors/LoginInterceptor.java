package atlas.infra.Interceptors;

import atlas.cotrole.SistemaController;
import atlas.infra.Annotations.Liberado;
import atlas.infra.UserSession;
import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.caelum.vraptor.resource.ResourceMethod;

/**
 *
 * @author Maison Chaves
 */
@Intercepts
@RequestScoped
public class LoginInterceptor implements Interceptor
{
    private final Result result;
    private final UserSession userSession;

    /**
     *
     * @param result
     * @param use
     */
    public LoginInterceptor(Result result, UserSession userSession)
    {
        this.result = result;
        this.userSession = userSession;
    }

    /**
     *
     * @param stack
     * @param method
     * @param resourceInstance
     * @throws InterceptionException
     */
    @Override
    public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance) throws InterceptionException
    {
        result.redirectTo(SistemaController.class).loginForm();
    }

    /**
     *
     * @param method
     * @return
     */
    @Override
    public boolean accepts(ResourceMethod method)
    {
        return !userSession.isLogado() && !method.containsAnnotation(Liberado.class);
        //return false;
    }
}
