package atlas.dao;

import atlas.modelo.Modulo;
import br.com.caelum.vraptor.ioc.Component;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Clase de teste para uma DAO extendido apartir de uma DAO generico
 *
 * @author Maison Chaves
 */
@Component
public class ModuloDAO extends AbstractBuscaDAO<Modulo>
{
    /**
     *
     * @param session
     */
    public ModuloDAO(Session session)
    {
        super(session);
    }

    /**
     *
     * @param numero
     * @return
     */
    public List<Modulo> busca(String numero)
    {
        return getSession().createCriteria(Modulo.class).add(Restrictions.eq("nome", numero)).list();
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Modulo> busca(Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(Modulo.class);

        criteria.addOrder(Order.asc("nome"));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }

    /**
     * Checa se não existe outra modulo como o mesmo nome excuindo ela
     * propria.
     *
     * @param modulo
     * @return
     */
    public Boolean check(Modulo modulo)
    {
        Criteria check = getSession().createCriteria(Modulo.class);
        if (modulo != null)
        {
            if (modulo.getNome()!= null)
            {
                check.add(Restrictions.eq("nome", modulo.getNome()));
            }
            if (modulo.getId()!= null && modulo.getId() != 0)
            {
                check.add(Restrictions.ne("id", modulo.getId()));
            }
        }

        return check.list().isEmpty();
    }

    @Override
    protected Criteria criteriosBusca(Modulo modulo, Criteria criteria)
    {
        if (modulo != null)
        {
            if (modulo.getNome()!= null && !modulo.getNome().equals(""))
            {
                criteria.add(Restrictions.eq("nome", modulo.getNome()));
            }
        }
        return criteria;
    }

    @Override
    protected Criteria ordem(Criteria criteria)
    {
        return criteria.addOrder(Order.asc("nome"));
    }
}
