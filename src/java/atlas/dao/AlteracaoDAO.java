package atlas.dao;

import atlas.modelo.Alteracao;
import br.com.caelum.vraptor.ioc.Component;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Clase de teste para uma DAO extendido apartir de uma DAO generico
 *
 * @author Maison Chaves
 */
@Component
public class AlteracaoDAO extends AbstractBuscaDAO<Alteracao>
{
    /**
     *
     * @param session
     */
    public AlteracaoDAO(Session session)
    {
        super(session);
    }

    /**
     *
     * @param numero
     * @return
     */
    public List<Alteracao> busca(Integer numero)
    {
        return getSession().createCriteria(Alteracao.class).add(Restrictions.eq("numero", numero)).list();
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Alteracao> busca(Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(Alteracao.class);

        criteria.addOrder(Order.asc("id"));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }

    @Override
    protected Criteria criteriosBusca(Alteracao alteracao, Criteria criteria)
    {
        if (alteracao != null)
        {
            if (alteracao.getTarefa()!= null && alteracao.getTarefa().getId() != 0)
            {
                criteria.add(Restrictions.eq("tarefa", alteracao.getTarefa()));
            }
        }
        return criteria;
    }

    @Override
    protected Criteria ordem(Criteria criteria)
    {
        return criteria.addOrder(Order.asc("nome"));
    }
}
