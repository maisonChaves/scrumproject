package atlas.dao;

import atlas.modelo.Tarefa;
import br.com.caelum.vraptor.ioc.Component;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Clase de teste para uma DAO extendido apartir de uma DAO generico
 *
 * @author Maison Chaves
 */
@Component
public class TarefaDAO extends AbstractBuscaDAO<Tarefa>
{
    /**
     *
     * @param session
     */
    public TarefaDAO(Session session)
    {
        super(session);
    }

    /**
     *
     * @param numero
     * @return
     */
    public List<Tarefa> busca(Integer numero)
    {
        return getSession().createCriteria(Tarefa.class).add(Restrictions.eq("numero", numero)).list();
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Tarefa> busca(Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(Tarefa.class);

        criteria.addOrder(Order.asc("id"));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }

    @Override
    protected Criteria criteriosBusca(Tarefa tarefa, Criteria criteria)
    {
        if (tarefa != null)
        {
            if (tarefa.getHistoria()!= null && tarefa.getHistoria().getId() != 0)
            {
                criteria.add(Restrictions.eq("historia", tarefa.getHistoria()));
            }
        }
        return criteria;
    }

    @Override
    protected Criteria ordem(Criteria criteria)
    {
        return criteria.addOrder(Order.asc("descricao"));
    }
}
