package atlas.dao;

import atlas.modelo.Diretorio;
import br.com.caelum.vraptor.ioc.Component;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Clase de teste para uma DAO extendido apartir de uma DAO generico
 *
 * @author Maison Chaves
 */
@Component
public class DiretorioDAO extends AbstractBuscaDAO<Diretorio>
{
    /**
     *
     * @param session
     */
    public DiretorioDAO(Session session)
    {
        super(session);
    }

    /**
     *
     * @param nome
     * @return
     */
    public List<Diretorio> busca(String nome)
    {
        return getSession().createCriteria(Diretorio.class).add(Restrictions.eq("nome", nome)).list();
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Diretorio> busca(Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(Diretorio.class);

        criteria.addOrder(Order.asc("nome"));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }

    /**
     * Checa se não existe outra diretorio como o mesmo nome excuindo ela
     * propria.
     *
     * @param diretorio
     * @return
     */
    public Boolean check(Diretorio diretorio)
    {
        Criteria check = getSession().createCriteria(Diretorio.class);
        if (diretorio != null)
        {
            if (diretorio.getNome()!= null)
            {
                check.add(Restrictions.eq("nome", diretorio.getNome()));
                check.add(Restrictions.eq("pai", diretorio.getPai()));
            }
            if (diretorio.getId()!= null && diretorio.getId() != 0)
            {
                check.add(Restrictions.ne("id", diretorio.getId()));
            }
        }

        return check.list().isEmpty();
    }

    @Override
    protected Criteria criteriosBusca(Diretorio diretorio, Criteria criteria)
    {
        if (diretorio != null)
        {
            if (diretorio.getNome()!= null && !diretorio.getNome().equals(""))
            {
                criteria.add(Restrictions.eq("nome", diretorio.getNome()));
            }
        }
        return criteria;
    }

    @Override
    protected Criteria ordem(Criteria criteria)
    {
        return criteria.addOrder(Order.asc("nome"));
    }
}
