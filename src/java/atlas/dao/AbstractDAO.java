package atlas.dao;

import br.com.caelum.vraptor.ioc.Component;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;

/**
 * Classe de DAO generico.
 *
 * @param <T> Classe que sera manipulada no banco
 * @author Maison Chaves
 */
@Component
public abstract class AbstractDAO<T extends Serializable>
{
    private final Session session;
    private Class<T> persistentClass;
    private Logger logger;

    /**
     * Construtor padrão
     *
     * @param session
     */
    protected AbstractDAO(Session session)
    {
        this.logger = Logger.getLogger( AbstractDAO.class.getName() );
        this.session = session;
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Retorna a sesssao ativa
     *
     * @return
     */
    public Session getSession()
    {
        return session;
    }

    /**
     * Recupera a classe que esta sendo manipulada
     *
     * @return
     */
    public Class<T> getPersistentClass()
    {
        return persistentClass;
    }

    /**
     * retorna todos os registros da entidade
     *
     * @return
     */
    public List<T> listaTudo()
    {
        logger.info("Metodo lista tudo");
        return this.session.createCriteria(this.getPersistentClass()).list();
    }

    /**
     * Salva o objeto na persistencia
     *
     * @param objeto
     * @throws HibernateException
     */
    public Long salva(T objeto) throws HibernateException
    {
        logger.info("Metodo salvar");
        Transaction tx = null;
        try
        {
            tx = session.beginTransaction();
            session.save(objeto);
            tx.commit();
        }
        catch (Exception e)
        {
            logger.error("Erro ao salvar instancia");
            if (tx != null)
            {
                try
                {
                    System.out.println("Salva - " + persistentClass.getCanonicalName());
                    System.out.println("Erro" + e.getMessage());
                    logger.info("Tenta dadar callback");
                    tx.rollback();
                }
                catch (HibernateException he)
                {
                    System.out.println("Salva - " + persistentClass.getCanonicalName());
                    System.out.println("Erro" + he.getMessage());
                    logger.error("Erro ao realizar callbck");
                }
            }
        }
        return null;
    }

    /**
     * Altera o objeto na Persistencia
     *
     * @param objeto
     * @throws HibernateException
     */
    public void altera(T objeto) throws HibernateException
    {
        logger.info("Metodo alterar");
        Transaction tx = null;
        try
        {
            tx = session.beginTransaction();
            session.update(objeto);
            tx.commit();
        }
        catch (Exception e)
        {
            logger.error("Erro ao alterar instancia");
            if (tx != null)
            {
                try
                {
                    System.out.println("Altera - " + persistentClass.getCanonicalName());
                    System.out.println("Erro" + e.getMessage());
                    logger.info("Tenta dadar callback");
                    tx.rollback();
                }
                catch (HibernateException he)
                {
                    System.out.println("Altera - " + persistentClass.getCanonicalName());
                    System.out.println("Erro" + he.getMessage());
                    logger.error("Erro ao realizar callbck");
                }
            }
        }
    }

    /**
     * Remove o objeto da persistencia
     *
     * @param objeto
     * @throws HibernateException
     */
    public void remove(T objeto) throws HibernateException
    {
        logger.info("Metodo remover");
        Transaction tx = null;
        try
        {
            tx = session.beginTransaction();
            session.delete(objeto);
            tx.commit();
        }
        catch (Exception e)
        {
            logger.error("Erro ao alterar instancia");
            if (tx != null)
            {
                try
                {
                    System.out.println("Remove - " + persistentClass.getCanonicalName());
                    System.out.println("Erro" + e.getMessage());
                    logger.info("Tenta dadar callback");
                    tx.rollback();
                }
                catch (HibernateException he)
                {
                    logger.error("Erro ao realizar callbck");
                    System.out.println("Remove - " + persistentClass.getCanonicalName());
                    System.out.println("Erro" + he.getMessage());
                }
            }
        }
    }

    /**
     * Recupera o objeto com base no codigo
     *
     * @param id codigo do objeto
     * @return
     */
    public T carrega(Long id)
    {
        logger.info("Metodo carrega");
        return (T) session.load(this.getPersistentClass(), id);
    }

    /**
     * Recupera o numero total de registros da classe
     *
     * @return
     */
    public Integer getTotalRegistros()
    {
        logger.info("Metodo get Total de Registros");
        Criteria criteria = this.session.createCriteria(this.getPersistentClass());

        criteria.setProjection(Projections.rowCount());
        return (Integer) criteria.uniqueResult();
    }
}
